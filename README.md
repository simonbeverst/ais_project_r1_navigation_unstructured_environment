# AIS Project R1: Navigation in unstructured environment

Autonomous Intelligent System Project R1: Navigation in an unstructured environment.
The implementation was done on a Ubuntu 20.04 LTS Desktop virtual machine in the ROS Noetic version.


## Installation
For installing our project please follow these steps:

**1.** Make sure you are using Ubuntu 20.04 LTS Desktop with ROS Noetic. For more details on installing Ubuntu and ROS read this [Tutorial](https://emanual.robotis.com/docs/en/platform/turtlebot3/quick-start/).


**2.** For setting your IP address and your Turtlebot model open the **.bashrc** with: 

`nano ~/.bashrc`

Then add these lines and replace _YOUR_IP_ with the IP of your remote PC:

`export ROS_MASTER_URI=http://YOUR_IP:11311`

`export ROS_HOSTNAME=YOUR_IP`

If you are using a Turtlebot Burger add this command:

`export TURTLEBOT3_MODEL = burger`

Afterwards you have to open a new terminal for the **.bashrc** being executed.


**3.** Flash the Raspberry of your Turtlebot3 Burger and setup the configurations properly as described in this [Setup](https://emanual.robotis.com/docs/en/platform/turtlebot3/sbc_setup/). 



**4.** Clone this repository to the src folder of your catkin workspace and build it.



**5.** Run the .launch files as described in **Usage** below.



## Content

The my_slam_package contains three launch files that are briefly described in this section. For more information on how to use them please scroll down to section **Usage**. 


| File | Function |
| ------ | ------ |
| my_slam_simulation.launch | This launch file is used for simulating the Turtlebot3 in Gazebo and performing a SLAM simulation in RViz |
| my_slam_package.launch | his launch file is used for executing SLAM methods in a real environment |
| my_slam_navigation | For navigating the Turtlebot3 in a real environment on the basis of your previous created Occupancy Grid Map |


***

## Usage

This section is about how to use the .launch files of this package.

### my_slam_simulation.launch
This launch file is used for simulating the Turtlebot3 in Gazebo and performing a SLAM simulation in RViz. When it is executed, Gazebo and RViz will open as well as the teleoperation node for controlling the Turtlebot in a separate terminal. For launching enter this command:

`roslaunch my_slam_package my_slam_simulation.launch slam_methods:=gmapping`

If you want to use another slam method just replace the argument _gmapping_ with _karto_, _hector_ or _cartographer_. 

### my_slam_package.launch
This launch file is used for executing SLAM methods in a real environment. To start it further precautions have to be taken. First connect to your Turtlebot3 with your remote PC via SSH connection. 

Use the following command and replace _TURTLEBOT_IP_ with the IP of your turtlebot:

`ssh ubuntu@TURTLEBOT_IP`

By entering the password **turtlebot** you are now able to launch files from the Turtlebot3.  Make sure roscore is already running on your remote PC before the next step. Now you have launch the turtlebot3_bringup via the ssh connection:


`roslaunch turtlebot3_bringup turtlebot3_robot.launch`


Now that the connection is established, the slam node of the package can be started:


`roslaunch my_slam_package my_slam_package.launch slam_methods:=gmapping`


The command will open Gazebo, RViz as well as a teleoperation node in a seperate window. As in the simulation node you can change the slam method by replacing the argument _gmapping_ with _karto_, _hector_ or _cartographer_.
Now you are able to move the Turtlebot3 through your room/area. While the Turtlebot3 is moving an Occupancy Grid Map (OGM) is built in RViz. When the map is complete you have to save your map by using the map server as you can see below. Remember to replace _YOUR_MAP_NAME_ with the map name you want.

`rosrun map_server map_saver -f ~/YOUR_MAP_NAME`

Since the location is given as **~/** the map will be saved in your **HOME** folder. This location can be adjusted if needed.

### my_slam_navigation

For navigating the Turtlebot3 in a real environment on the basis of your previous created Occupancy Grid Map. Before running this node the roscore must run. Then the turtlebot3_bringup has to be started via ssh like explained in the previous section. Afterwards this node can be launched by using the following command where _YOUR_MAP_NAME_ must be replaced with the name of your Occupancy Grid Map which must be in your **HOME** folder. 


`roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=$HOME/YOUR_MAP_NAME.yaml`

Now RViz will open with the given map. After you did the **2D Pose Estimation** to determine the starting location and orientation of the Turtlebot3 in the Map, you can specify a target vector that contains wanted location and orientation to which the Turtlebot3 should move by using **2D Nav Goal**.

***


## Authors and acknowledgment
The authors of this project are Miguel Ferber (miguel.ferber@stud.fra-uas.de) and Simon Beverst(simon.beverst@stud.fra-uas.de). Both worked on this thesis as part of their mechatronics and robotics master studies.


